/**
 * Copyright 2013 C. A. Fitzgerald
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.github.riotopsys.kanatrainer.helper;

import java.util.HashMap;
import java.util.Map;

import com.github.riotopsys.kanatrainer.R;

import android.content.Context;
import android.media.SoundPool;

public class StringIndexedSoundPool extends SoundPool {
	
	private Map<String, Integer> idXref = new HashMap<String, Integer>();

	public StringIndexedSoundPool(int maxStreams, int streamType, int srcQuality) {
		super(maxStreams, streamType, srcQuality);
	}
	
	public boolean contains(String id){
		return idXref.containsKey(id);
	}
	
	public int load( Context ctx, String id ){
		if ( idXref.containsKey(id) ){
			return idXref.get(id);
		} 
		int rawId = ctx.getResources().getIdentifier(id, "raw", R.class.getPackage().getName());
		int result =load(ctx,rawId,1);
		if ( result != 0){
			idXref.put(id, result);
		}
		return result;
	}
	
	public int play( String id ){
		if ( idXref.containsKey(id) ){
			return play(idXref.get(id),1f,1f,0,0,1f);
		}
		return 0;
	}

}
