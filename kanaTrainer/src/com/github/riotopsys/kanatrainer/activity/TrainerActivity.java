/**
 * Copyright 2013 C. A. Fitzgerald
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.github.riotopsys.kanatrainer.activity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.github.riotopsys.hexgrid.HexGrid;
import com.github.riotopsys.kanatrainer.R;
import com.github.riotopsys.kanatrainer.animation.AnimationStarter;
import com.github.riotopsys.kanatrainer.animation.HideView;
import com.github.riotopsys.kanatrainer.helper.StringIndexedSoundPool;
import com.github.riotopsys.kanatrainer.training.Question;
import com.github.riotopsys.kanatrainer.training.Trainer;

public class TrainerActivity extends Activity implements OnTouchListener,
		OnDragListener, OnLoadCompleteListener, OnClickListener,
		OnPreDrawListener {

	private static final String TRAINER_KEY = "game";

	private View landingArea;

	private Trainer train;

	private StringIndexedSoundPool soundPool = new StringIndexedSoundPool(1,
			AudioManager.STREAM_MUSIC, 0);

	private View replay;

	private View root;

	private HexGrid answerGrid;

	private int candidates;

	private boolean correct = true;
	
	private Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trainer);

		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

		landingArea = findViewById(R.id.landing_area);
		replay = findViewById(R.id.replay);
		answerGrid = (HexGrid) findViewById(R.id.answer_grid);

		root = findViewById(R.id.trainer_activity);
		root.getViewTreeObserver().addOnPreDrawListener(this);

		landingArea.setOnDragListener(this);
		replay.setOnClickListener(this);

		if (savedInstanceState != null) {
			train = (Trainer) savedInstanceState.getSerializable(TRAINER_KEY);
		} else {
			newGame();
		}

		soundPool.setOnLoadCompleteListener(this);
		
	}

	@Override
	protected void onPause() {
		soundPool.autoPause();
		super.onPause();
	}
	
	public void onSaveInstanceState(Bundle savedInstanceState) {

		savedInstanceState.putSerializable(TRAINER_KEY, train);

		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_trainer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menuRestart:
			newGame();
			setupCurrentQuestion();
			break;
		case R.id.menuSettings:
			Intent i = new Intent(this, Preference.class);
			startActivity(i);
			break;
		}
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			ClipData data = ClipData.newPlainText("", "");
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
			v.startDrag(data, shadowBuilder, v, 0);
			v.setVisibility(View.INVISIBLE);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean onDrag(View v, DragEvent event) {
		View view = (View) event.getLocalState();
		switch (event.getAction()) {
		case DragEvent.ACTION_DRAG_STARTED:
			landingArea.setBackgroundResource(R.drawable.animated_circle);
			AnimationDrawable background = (AnimationDrawable) landingArea.getBackground();
			background.start();
			break;
		case DragEvent.ACTION_DRAG_ENTERED:
			break;
		case DragEvent.ACTION_DRAG_EXITED:
			break;
		case DragEvent.ACTION_DROP:
			String tag = (String) view.findViewById(R.id.answer_text).getTag();
			if ( !correct ){
				if ( train.currentQuestion().romaji.equals(tag) ){
					train.nextQuestion();
					setupCurrentQuestion();
					playCurrentQuestion();
					correct = true;
					for ( int c = 0; c < answerGrid.getChildCount(); c++ ){
						answerGrid.getChildAt(c).setVisibility(View.VISIBLE);
					}
				} 
				return true;
			}
			
			correct = train.check(tag);
			if ( correct ){
				//correct!
				train.nextQuestion();
				setupCurrentQuestion();
			} else {
				TextView textView = (TextView) answerGrid.findViewWithTag(tag);
				textView.setBackgroundResource(R.drawable.disabled_circle);
				textView.setTextColor( getResources().getColor(android.R.color.holo_red_dark) );
				
				TextView correct = (TextView) answerGrid.findViewWithTag(train.currentQuestion().romaji);
				correct.setBackgroundResource(R.drawable.correct_circle);
				correct.setTextColor( getResources().getColor(android.R.color.holo_green_light) );
				
				List<View> dropItems = new LinkedList<View>();
				for ( int c = 0; c < answerGrid.getChildCount(); c++ ){
					dropItems.add(answerGrid.getChildAt(c));
				}
				dropItems.remove(textView.getParent());
				dropItems.remove(correct.getParent());
				
				long delayMillis = 0;
				Collections.shuffle(dropItems);
				for ( View drop : dropItems ){
					delayMillis += random(1,6) + random(1,6) + random(1,6);
					Animation dropAnimation = AnimationUtils.loadAnimation(this, R.animator.drop);
					dropAnimation.setAnimationListener(new HideView( drop ));
					handler.postDelayed(new AnimationStarter(dropAnimation, drop ), delayMillis);
				}
				
			}
			playCurrentQuestion();

			break;
		case DragEvent.ACTION_DRAG_ENDED:
			view.setVisibility(View.VISIBLE);
			landingArea.setBackgroundResource(R.drawable.circle);
		default:
			break;
		}
		return true;
	}

	private void newGame() {
		train = new Trainer(buildQuestions());
		playCurrentQuestion();
	}

	private void setupCurrentQuestion() {
		List<Question> active = train.getAnswers(candidates);
		for ( int c = 0; c < answerGrid.getChildCount(); c++){
			Question q = active.get(c);
			TextView textView = (TextView) answerGrid.getChildAt(c).findViewById(R.id.answer_text);
			textView.setText(q.kana);
			textView.setTag(q.romaji);
			textView.setBackgroundResource(R.drawable.circle);
			textView.setTextColor( getResources().getColor(android.R.color.white) );
		}
	}

	private void playCurrentQuestion() {
		Question currentQ = train.currentQuestion();
		if (soundPool.contains(currentQ.romaji)) {
			soundPool.play(currentQ.romaji);
		} else {
			soundPool.load(this, currentQ.romaji);
		}
	}

	private Question[] buildQuestions() {

		SharedPreferences perfs = PreferenceManager
				.getDefaultSharedPreferences(this);

		LinkedList<Question> qs = new LinkedList<Question>();
		Question[] result = new Question[1];

		Resources res = this.getResources();
		String[] roma = res.getStringArray(R.array.romaji);
		String[] romaDia = res.getStringArray(R.array.romaji_diacritics);
		String[] kana;

		String fred = perfs.getString("include", res.getString(R.string.hira));

		if (fred.equals(res.getString(R.string.hira)) || fred.equals("Both")) {
			kana = res.getStringArray(R.array.hiragana);

			for (int c = 0; c < kana.length; c++) {
				qs.add(new Question(kana[c], roma[c]));
			}
			if (perfs.getBoolean("diacritics", false)) {
				kana = res.getStringArray(R.array.hiragana_diacritics);
				for (int c = 0; c < kana.length; c++) {
					qs.add(new Question(kana[c], romaDia[c]));
				}
			}
		}

		if (fred.equals(res.getString(R.string.kata)) || fred.equals("Both")) {
			kana = res.getStringArray(R.array.katakana);

			for (int c = 0; c < kana.length; c++) {
				qs.add(new Question(kana[c], roma[c]));
			}
			if (perfs.getBoolean("diacritics", false)) {
				kana = res.getStringArray(R.array.katakana_diacritics);
				for (int c = 0; c < kana.length; c++) {
					qs.add(new Question(kana[c], romaDia[c]));
				}
			}
		}

		return qs.toArray(result);
	}

	@Override
	public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
		soundPool.play(sampleId, 1f, 1f, 0, 0, 1f);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.replay:
			playCurrentQuestion();
			break;
		}
	}

	@Override
	public boolean onPreDraw() {
		root.getViewTreeObserver().removeOnPreDrawListener(this);
		candidates = answerGrid.getSupportedChildern();
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (int a = 0; a < candidates; a++) {
			View v = inflater.inflate(R.layout.circled_glyph, null);
			v.setOnTouchListener(this);
			answerGrid.addView(v);
		}
		setupCurrentQuestion();
		return false;
	}

	private long random( long min, long max) {
		return min + (int)(Math.random() * ((max - min) + 1));
	}

}
